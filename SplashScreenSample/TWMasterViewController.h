//
//  TWMasterViewController.h
//  SplashScreenSample
//
//  Created by Senthil Kumar on 28/11/12.
//  Copyright (c) 2012 ThoughtWorks. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TWDetailViewController;

@interface TWMasterViewController : UITableViewController

@property (strong, nonatomic) TWDetailViewController *detailViewController;

@end
