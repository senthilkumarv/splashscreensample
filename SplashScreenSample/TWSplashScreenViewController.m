//
//  TWSplashScreenViewController.m
//  SplashScreenSample
//
//  Created by Senthil Kumar on 28/11/12.
//  Copyright (c) 2012 ThoughtWorks. All rights reserved.
//

#import "TWSplashScreenViewController.h"

@interface TWSplashScreenViewController ()

@end

@implementation TWSplashScreenViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

@end
