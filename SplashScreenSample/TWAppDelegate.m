//
//  TWAppDelegate.m
//  SplashScreenSample
//
//  Created by Senthil Kumar on 28/11/12.
//  Copyright (c) 2012 ThoughtWorks. All rights reserved.
//

#import "TWAppDelegate.h"

#import "TWMasterViewController.h"
#import "TWSplashScreenViewController.h"

@implementation TWAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    TWMasterViewController *masterViewController = [[TWMasterViewController alloc] initWithNibName:@"TWMasterViewController" bundle:nil];
    self.navigationController = [[UINavigationController alloc] initWithRootViewController:masterViewController];
    self.window.rootViewController = self.navigationController;
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [self showSplashScreen];
}

-(void) showApplication {
    [self.navigationController dismissViewControllerAnimated:NO completion:^{
        NSLog(@"Showing main view");
    }];
}

- (void) showSplashScreen {
    TWSplashScreenViewController *splashScreenViewController = [[TWSplashScreenViewController alloc] initWithNibName:@"TWSplashScreenViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController presentViewController:splashScreenViewController animated:NO completion:^{
        NSLog(@"Doing some heavy lifting that will take 10 seconds to compelete");
        [self performSelector:@selector(showApplication) withObject:nil afterDelay:10];
    }];
}
@end
